from flask import Flask, render_template, session, redirect
from functools import wraps
import pymongo

app = Flask(__name__)
app.secret_key = b'\x98\xe7@\xa3\x9d\xf3\xfbp\xd4\x9a\xe8u9u\x96\xc9'

# # Database
client = pymongo.MongoClient('localhost', 27017)
db = client.polpp

# # Decorators
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect('/')
    
    return wrap

# Routes
from user import routes

@app.route('/')
def home():
    return render_template('login.html')


@app.route('/dashboard/')
@login_required
def dashboard():
    return render_template('dashboard.html')
