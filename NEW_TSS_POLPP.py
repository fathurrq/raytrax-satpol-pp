import cv2
import numpy as np
import time
from matplotlib import pyplot as plt
import datetime


net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))
cap = cv2.VideoCapture('test.mp4')
font = cv2.FONT_HERSHEY_PLAIN
starting_time = time.time()
frame_id = 0
line = [(732,500), (1249,549)]

while True:
    incount1 = 0
    
    _, frame = cap.read()
    frame = cv2.resize(frame, (640,480))
    
    frame_id += 1
    height, width, channels = frame.shape
    blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), [0, 0, 0, 0], True, crop=False)
    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.2:
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2.5)
                y = int(center_y - h / 2.5)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes (boxes, confidences, 0.2, 0.2)
    for i in range(len(boxes)) :
        if i in indexes :
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            if class_ids[i] == [0] and 0 < x < 636 and 478 > y > 0 :
                incount1 += 1    
            confidence = confidences[i]
            color = colors[class_ids[i]]
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            cv2.circle(frame, (int((w/2) + x), int((h/2)+y)), 2, (0,0,255), -1)
            cv2.putText(frame, label , (x, y + 30), font, 1, color, 2)



   
    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time
    cv2.putText(frame, "Person =" + str(incount1), (10,120),font,1, (0,0,0), 1 )
    


    cv2.imshow("Image",frame)
    # plt.imshow(frame)
    # plt.show()
    key = cv2.waitKey(1)
    if key == 27:
        break
cap.release()
cv2.destroyAllWindows()
