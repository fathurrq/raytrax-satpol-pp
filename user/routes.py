from flask import Flask
from app import app
from user.models import User

@app.route('user/login', methods=['POST'])
def login():
    return User().login()

@app.route('user/logout')
def signout():
    return User().signout()