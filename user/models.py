from flask import Flask, jsonify, redirect, request, session, redirect
from app import db

class User:
    
    def start_session(self):
        session['logged_in'] = True
        return "berhasil login", 200


    def login(self):

        user = db.find.find_one({
            "username": request.form.get('username')
        })

        if user:
            if(request.form.get('pass') == user['pass']):
                return self.start_session()

        return jsonify({"error": "pass atau uname salah"}), 401


    def signout(self):
        session.clear()
        return redirect('/')

